﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OpenAI.Helper;

namespace OpenAI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OpenAIController : ControllerBase
    {
        [HttpGet]
        public async Task <string> Message(string message)
        {
            IOpenAIProxy chatOpenAI = new OpenAIProxy(
                apiKey: "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",
                organizationId: "org-b7iySEqDeeQBqy8WzTLJpEyL");
                var results = await chatOpenAI.SendChatMessage(message);
                 var result = "";
                foreach (var item in results)
                {
                   result += $"{item.Role}: {item.Content}";
                }

                return result;
        }
    }
}
