﻿using Standard.AI.OpenAI.Models.Services.Foundations.ChatCompletions;
namespace OpenAI.Helper
{
    public interface IOpenAIProxy
    {
        Task<ChatCompletionMessage[]> SendChatMessage(string message);
    }
}
